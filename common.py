"""Common methods for admin and user"""

from datetime import datetime
import itertools
from flask import abort, render_template
from api import get_partner, get_receipt, get_receipts_by_email_and_year
from service import produce_multiple_receipts_pdf


def produce_grouped_receipts(email, year):
    """Produce grouped recipients PDF"""
    (user_receipts, donor) = get_donor_from_email_and_year(email, year)
    if donor["email"] == email:
        return produce_multiple_receipts_pdf(user_receipts, year)
    return abort(404)


def get_donor_from_receipt_id(receipt_id):
    """Returns donor information from a receipt ID"""
    receipt_res = get_receipt(receipt_id)
    if len(receipt_res) < 1:
        return abort(404)
    first_receipt = receipt_res[0]
    return (get_partner(first_receipt["partner_id"][0])[0], first_receipt)


def get_donor_from_email_and_year(email, year):
    """Get donor information from email and year"""
    user_receipts = get_receipts_by_email_and_year(email, year)
    if len(user_receipts) < 1:
        return abort(404)
    first_receipt = user_receipts[0]
    return (user_receipts, __get_donor_from_receipt(first_receipt))


def produce_receipt_list(partners, user_receipts, token=None):
    """Produce the rendered list of receipts for a user"""
    partner = partners[0]
    # Reorder the receipts by donation date,
    # as the receipts can be created a longtime after the donation date
    user_receipts.sort(
        key=lambda element: datetime.strptime(element["donation_date"], "%Y-%m-%d").year,
        reverse=True,
    )
    grouped_user_receipts = itertools.groupby(
        user_receipts,
        key=lambda element: datetime.strptime(element["donation_date"], "%Y-%m-%d").year,
    )
    return render_template(
        "receipts.html",
        donor=partner,
        grouped_receipts=grouped_user_receipts,
        receipts=user_receipts,
        partners=partners,
        is_admin=token is None,
        token=token,
    )


def __get_donor_from_receipt(receipt):
    return get_partner(receipt["partner_id"][0])[0]
