# Beleg

Beleg est un outil pour se connecter à un serveur Odoo, récupérer les donations, générer des reçus de dons et permettre aux donateur·ices de les récupérer.

## Installation

Beleg has been tested on Python 3.9 and 3.11.

```sh
git clone git@framagit.org:framasoft/beleg.git && cd beleg # Clone the repo
python3 -m venv .venv # Create the venv, optionnaly with the python command instead of python3, depending on your system
. .venv/bin/activate # Activate the venv
pip install -r requirements.txt # Install deps
cp .env.template .env # Copy env template to actual env file
# Fill the .env file keys with appropriate values
flask --app beleg --debug init-db # Create the database
flask --app beleg --debug run # Run the flask build-in server
```

Server should be available on http://localhost:5000

Flask documentation: https://flask.palletsprojects.com

## Production

Instead of running the build-in server, use Gunicorn. Once the `venv` is activated, run `pip install gunicorn` then use the `support/systemd/beleg.service` file as a systemd service unit. Gunicorn runs on port 8000, so you need to point your reverse proxy to this port. Don't forget to add appropriate headers as told in the Flask documentation (see below).

Reference: https://flask.palletsprojects.com/en/2.3.x/deploying/

