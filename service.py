"""Various services"""

from datetime import datetime, timedelta
from os import getenv
from flask import make_response
from api import get_donation, get_partner
from db import get_db
from pdf import PDFException, return_pdf_receipt


def produce_dons_pdf(receipt):
    """Produce an HTTP response with the PDF of donations"""
    pdf = pre_produce_dons(receipt)
    response = make_response(pdf.write_pdf(None))
    response.headers["Content-Type"] = "application/pdf"
    receipt_id = receipt["id"]
    filename = f"Reçu des dons à Framasoft n°{receipt_id}"
    response.headers["Content-Disposition"] = f"inline; filename={filename}.pdf"
    return response


def pre_produce_dons(receipt, donations=None):
    """Return the PDF file for a receipt"""
    donor = get_partner(receipt["partner_id"][0])[0]

    donations = donations if donations else donations_for_receipt(receipt)
    if not donations:
        return None

    template = "pdf/dons entreprise.html" if donor["is_company"] is True else "pdf/dons.html"
    return return_pdf_receipt(template, receipt=receipt, donations=donations, donor=donor)


def generate_multiple_receipts_pdf(receipts, year):
    """Return the PDF file for multiple receipts"""
    (total_amount, nb_valid_donations) = total_donations_for_receipts(receipts)
    try:
        pdf_pages = [
            return_pdf_receipt(
                "pdf/intro.html",
                nb_receipts=nb_valid_donations,
                total_amount=total_amount,
                year=year,
            )
        ]
        for receipt in receipts:
            page = pre_produce_dons(receipt)
            if page is not None:
                pdf_pages.append(page)
        all_pages = [p for doc in pdf_pages for p in doc.pages]
        return pdf_pages[0].copy(all_pages).write_pdf(None)
    except Exception as e:
        raise PDFException("PDF generation issue") from e


def produce_multiple_receipts_pdf(receipts, year):
    """Generates an HTTP response with multiple receipts PDF file"""
    pdf = generate_multiple_receipts_pdf(receipts, year)
    response = make_response(pdf)
    filename = f"Reçu des dons à Framasoft pour l'année {year}"
    response.headers["Content-Type"] = "application/pdf"
    response.headers["Content-Disposition"] = f"inline; filename={filename}.pdf"
    return response


def save_token(email, token):
    """Save a receipt token in the database"""
    get_db().cursor().execute("INSERT INTO tokens VALUES (?, ?, ?)", [token, email, datetime.now()])
    get_db().commit()


def email_from_token(token):
    """Retreive email from a token in the database"""
    cur = get_db().execute("SELECT email from tokens WHERE token = ?", [token])
    row = cur.fetchone()
    if row:
        return row["email"]
    return None


def delete_obsolete_tokens():
    """Delete obsolete tokens"""
    delay = int(getenv("DELAY_BEFORE_TOKEN_DELETION", "3600"))
    deletion_datetime = datetime.now() - timedelta(seconds=delay)
    get_db().cursor().execute("DELETE FROM tokens WHERE inserted_at < ?", [deletion_datetime])
    get_db().commit()


def donations_for_receipt(receipt):
    """Return the donations for a given receipt"""
    donations = []
    for donation_id in receipt["donation_ids"]:
        donations.append(get_donation(donation_id)[0])
    return donations


def total_donations_for_receipt(receipt):
    """Return the total amount in donations for a given receipt"""
    total = 0
    for donation in donations_for_receipt(receipt):
        total += donation["amount_total"]
    return total


def total_donations_for_receipts(receipts):
    """Return the total amount in donations for multiple receipts"""
    total = 0
    nb_valid_donations = 0
    for receipt in receipts:
        amount = total_donations_for_receipt(receipt)
        if amount > 0:
            total += amount
            nb_valid_donations += 1
    return (total, nb_valid_donations)


def save_year_sent_email(year, email):
    """Save to database an email being sent this year"""
    get_db().cursor().execute(
        "INSERT INTO emails_sent VALUES (?, ?, ?)", [year, email, datetime.now()]
    )
    get_db().commit()


def is_email_already_sent_for_year(year, email):
    """Whether an email has already being sent this year"""
    cur = (
        get_db()
        .cursor()
        .execute("SELECT email from emails_sent WHERE date_year = ? AND email = ?", [year, email])
    )
    row = cur.fetchone()
    if row:
        return True
    return False


def remove_emails_sent_for_year(year):
    """Remove all emails already sent this year"""
    get_db().cursor().execute("DELETE FROM emails_sent WHERE date_year = ?", [year])
    get_db().commit()


def remove_emails_sent_for_year_and_email(year, email):
    """Remove a given email already sent this year"""
    get_db().cursor().execute(
        "DELETE FROM emails_sent WHERE date_year = ? AND email = ?", [year, email]
    )
    get_db().commit()
