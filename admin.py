"""Provide admin section"""

from os import getenv
from datetime import datetime
from flask_httpauth import HTTPBasicAuth
from flask import Blueprint, abort, flash, render_template, request, redirect, url_for
from werkzeug.security import check_password_hash
from email_service import send_mail
from api import get_partners, get_receipt, get_receipts
from service import generate_multiple_receipts_pdf, pre_produce_dons, produce_dons_pdf
from common import (
    get_donor_from_email_and_year,
    get_donor_from_receipt_id,
    produce_grouped_receipts,
    produce_receipt_list,
)

bp = Blueprint("admin", __name__, url_prefix="/admin")
auth = HTTPBasicAuth()

users = {"admin": getenv("ADMIN_PASSWORD_HASH")}


@auth.verify_password
def verify_password(username, password):
    """Check the admin password"""
    if username in users and check_password_hash(users.get(username), password):
        return username
    return None


@bp.route("/", methods=["GET"])
@auth.login_required
def query():
    """Show the query form"""
    current_year = datetime.now().year
    return render_template("query.html", current_year=current_year)


@bp.route("/receipt", methods=["POST", "GET"])
@auth.login_required
def receipts():
    """Show the receipts page"""
    if request.method == "POST":
        email_donateur = request.form["email"].strip()
    elif request.args.get("email") is not None:
        email_donateur = request.args.get("email")
    else:
        return abort(404)

    user_receipts = get_receipts(email_donateur)
    partners = get_partners(email_donateur)
    if len(partners) > 1:
        flash(
            f"Nous avons trouvé {len(partners)} partenaires pour l'email {email_donateur}."
            "Il est probablement nécessaire de faire le ménage dans Odoo.",
            "warning",
        )
    if len(partners) == 0:
        return render_template(
            "error.html",
            details=f"Le courriel {email_donateur} n'est associé à aucun don "
            "dans notre base de données. "
            "Peut-être avez-vous fait une erreur en le renseignant ?",
            to_admin=True,
        ), 404
    return produce_receipt_list(partners, user_receipts)


@bp.route("/receipt/<receipt_id>", methods=["GET"])
@auth.login_required
def receipt(receipt_id):
    """Show the receipt page"""
    receipt_res = get_receipt(int(receipt_id))
    if len(receipt_res) < 1:
        return abort(404)
    first_receipt = receipt_res[0]
    return produce_dons_pdf(first_receipt)


@bp.route("/grouped_receipts/<year>", methods=["GET"])
@auth.login_required
def grouped_receipts(year):
    """Show the grouped receipts page"""
    email = request.args.get("email")
    return produce_grouped_receipts(email, year)


@auth.error_handler
def auth_error(status):
    """Show an auth error page"""
    return render_template("errors/auth.html"), status


@bp.route("/receipt/<receipt_id>/send", methods=["GET"])
@auth.login_required
def send_receipt(receipt_id):
    """Send a receipt by email"""
    (donor, first_receipt) = get_donor_from_receipt_id(int(receipt_id))
    email = donor["email"]
    pdf = pre_produce_dons(first_receipt).write_pdf(None)
    send_mail(
        email,
        f"Reçu des dons à Framasoft n°{receipt_id}",
        f"""
Bonjour,

Nous vous envoyons en pièce jointe votre reçu de don n°{receipt_id}.

Si vous n'êtes pas à l'origine de cette demande, vous pouvez ignorer ce courriel.

Merci de votre soutien,
L'équipe Framasoft""",
        files=[(pdf, "application", "pdf", f"Reçu des dons à Framasoft n°{receipt_id}.pdf")],
    )
    flash(f"Reçu n°{receipt_id} envoyé à {email}")
    return redirect(url_for("admin.receipts", email=email))


@bp.route("/grouped_receipts/<year>/send", methods=["GET"])
@auth.login_required
def send_grouped_receipts(year):
    """Send multiple receipts by email"""
    email = request.args.get("email")
    (user_receipts, donor) = get_donor_from_email_and_year(email, year)
    if donor["email"] == email:
        pdf = generate_multiple_receipts_pdf(user_receipts, year)
        send_mail(
            email,
            f"Reçu des dons à Framasoft pour l'année {year}",
            f"""
Bonjour,

Nous vous envoyons en pièce jointe l'ensemble des reçus pour vos dons en {year}.

Si vous n'êtes pas à l'origine de cette demande, vous pouvez ignorer ce courriel.

Merci de votre soutien,
L'équipe Framasoft""",
            files=[(pdf, "application", "pdf", f"Reçu dons Framasoft {year}.pdf")],
        )
        flash(f"Reçus pour {year} envoyés à {email}")
        return redirect(url_for("admin.receipts", email=email))
    return abort(404)
