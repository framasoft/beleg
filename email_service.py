"""Mailer tools"""

from email.message import EmailMessage
import logging
from os import getenv
from secrets import token_urlsafe
import smtplib
import css_inline
from flask import url_for, render_template
from filters import format_currency
from service import save_token


def send_mail(to, subject, message, reply_to="rt+soutenir@framasoft.org", files=None, html=None):
    """Send a generic mail"""
    if files is None:
        files = []
    from_email = getenv("EMAIL_FROM_ADDRESS")
    msg = EmailMessage()
    msg.set_content(message)
    msg["Subject"] = subject
    msg["From"] = from_email
    msg["To"] = to
    msg["Reply-To"] = reply_to

    for data, maintype, subtype, filename in files:
        msg.add_attachment(data, maintype=maintype, subtype=subtype, filename=filename)
        text_part, *_attachment_part = msg.iter_parts()

    if html:
        if len(files) > 0:
            text_part.add_alternative(css_inline.inline(html), subtype="html")  # pylint: disable=no-member
        else:
            msg.add_alternative(css_inline.inline(html), subtype="html")  # pylint: disable=no-member

    logging.info("Sending email from %s to %s: %s", from_email, to, subject)
    __do_send_mail(msg)


def __do_send_mail(message):
    smtp_server = getenv("SMTP_SERVER", "localhost")
    smtp_port = int(getenv("SMTP_PORT", "25"))
    enable_emails = getenv("ENABLE_EMAILS", "False")
    if enable_emails in ["True", "true", "1"]:
        try:
            with smtplib.SMTP(smtp_server, smtp_port) as server:
                server.send_message(message)
        except Exception as e:
            raise MailException("Mail sending issue") from e
    else:
        logging.info("Sending emails is disabled, skipped")


def send_donation_email(email_donateur, partner):
    """Send a donation mail"""
    token = token_urlsafe(16)
    save_token(email_donateur, token)
    display_name = partner["display_name"]
    url = url_for("user.receipts", token=token, _external=True)
    title = "Reçu des dons effectués à Framasoft"
    html = render_template("email/donation_request.html", url=url, title=title)
    send_mail(
        f"{display_name} <{email_donateur}>",
        title,
        f"""Bonjour,

Nous avons reçu une demande d'édition de la liste des dons effectués à Framasoft adressée à cette adresse de courriel.

Pour accéder à votre reçu des dons effectués à Framasoft, cliquez sur le lien suivant : {url}

Si vous n'êtes pas à l'origine de cette demande, vous pouvez ignorer ce courriel.

Merci de votre soutien,
L'équipe Framasoft
""",
        html=html,
    )


def send_nothing_found(email_donateur):
    """Send an email to say we've found nothing"""
    url = url_for("user.request_receipt", _external=True)
    title = "Reçu des dons effectués à Framasoft"
    html = render_template("email/donation_not_found.html", url=url, title=title)
    send_mail(
        email_donateur,
        title,
        f"""Bonjour,

Nous avons reçu une demande d'édition de la liste des dons effectués à Framasoft adressée à cette adresse de courriel.

Hélas, nous n'avons pas trouvé de dons effectués à Framasoft à partir de cette adresse de courriel. Assurez-vous que vous avez renseigné la bonne adresse.

Revenir au formulaire de demande d'édition de reçu : {url}

Si vous n'êtes pas à l'origine de cette demande, vous pouvez ignorer ce courriel.

L'équipe Framasoft - https://framasoft.org""",
        html=html,
    )


def send_yearly_donation_email(
    year, email_donateur, display_name, total_amount, nb_valid_donations, pdf
):
    """Send a yearly donation mail"""
    logging.debug(
        "Sending yearly donation email from %s to %s (%s)", year, display_name, email_donateur
    )
    year_int = int(year)
    title = f"Reçu des dons effectués à Framasoft en {year}"
    html = render_template(
        "email/yearly_receipts.html",
        year=year,
        year_int=year_int,
        display_name=display_name,
        title=title,
        total_amount=total_amount,
        nb_valid_donations=nb_valid_donations,
    )
    send_mail(
        f"{display_name} <{email_donateur}>",
        title,
        f"Bonjour {display_name},\n\n"
        f"En {year}, vous avez fait "
        f"{'un don' if nb_valid_donations == 1 else f'{nb_valid_donations} dons'} "
        f"à Framasoft, pour un montant total de {format_currency(total_amount)} : "
        "nous vous en remercions sincèrement.\n\n"
        "Le budget de Framasoft dépend à 96 % de vos dons, "
        "ce qui permet à l'association de garder une indépendance financière et "
        "une grande liberté dans ses actions. Et celles-ci ne seraient pas réalisables "
        "sans vous !\n\nAinsi, votre don contribue directement à l'ensemble de nos projets : "
        "de nos actions d'émancipation numérique https://framasoft.org/fr/empowerment "
        "(services en ligne, développement logiciels, contributions...) à nos interventions "
        "d'éducation populaire https://framasoft.org/fr/educ-pop (articles de blog, prises de "
        "parole, conférences...) en passant par diverses coopérations "
        "https://framasoft.org/fr/archipelago avec des projets portant nos valeurs.\n\n"
        "Nous vous rappelons que votre don ouvre droit à une réduction d'impôts "
        "(cf. <http://soutenir.framasoft.org/defiscalisation>). Vous trouverez ci-joint "
        "votre reçu fiscal, que nous vous invitons à conserver précieusement en cas de "
        "contrôle de la part des impôts. Vos reçus sont également récupérables sur le site "
        "https://recu-dons.framasoft.org \n\n"
        f"Attention, les dons effectués à compter du 1er janvier {year_int + 1} seront à déclarer "
        f"sur votre déclaration de revenus {year_int + 1} que vous recevrez en {year_int + 2}. "
        "Pour plus d'informations, rendez-vous sur la page "
        "<http://soutenir.framasoft.org/defiscalisation>. "
        "N'hésitez pas à nous contacter si vous avez des questions.\n\n"
        "Une nouvelle fois, merci beaucoup pour votre reconnaissance et votre "
        "précieux soutien.\n\n"
        "L'équipe Framasoft.\n\n"
        "Pour nous contacter, une seule adresse : https://contact.framasoft.org\n"
        "− « La route est longue mais la voie est libre... »",
        files=[(pdf, "application", "pdf", f"Reçu dons Framasoft {year}.pdf")],
        html=html,
    )


class MailException(Exception):
    "Raised when something goes wrong with sending e-mails"
