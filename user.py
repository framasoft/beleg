"""Provide user section"""

from flask import Blueprint, render_template, request, abort, flash, redirect, url_for
from email_service import send_donation_email, send_mail, send_nothing_found
from api import get_partners, get_receipts
from service import email_from_token, produce_dons_pdf
from common import get_donor_from_receipt_id, produce_grouped_receipts, produce_receipt_list

bp = Blueprint("user", __name__, url_prefix="/")


@bp.route("/", methods=["GET"])
def index():
    """The homepage"""
    return redirect("/request")


@bp.route("/request", methods=["GET", "POST"])
def request_receipt():
    """The form to request a receipt"""
    if request.method == "POST":
        email_donateur = request.form["email"].strip()

        partners = get_partners(email_donateur)
        if len(partners) == 0:
            send_nothing_found(email_donateur)
        else:
            partner = partners[0]
            try:
                if len(get_receipts(email_donateur)) > 0:
                    send_donation_email(email_donateur, partner)
                else:
                    send_nothing_found(email_donateur)
            except ConnectionRefusedError:
                return abort(500)

        flash(
            f"Nous avons envoyé un courriel contenant les informations "
            f"pour accéder au reçu à l'adresse {email_donateur}"
        )
    return render_template("request.html")


@bp.route("/receipts", methods=["GET"])
def receipts():
    """The user receipts"""
    token = request.args.get("token")
    email = email_from_token(token)
    if email:
        user_receipts = get_receipts(email)
        partners = get_partners(email)
        return produce_receipt_list(partners, user_receipts, token)
    return abort(404)


@bp.route("/receipt/<receipt_id>", methods=["GET"])
def receipt(receipt_id):
    """A PDF receipt"""
    token = request.args.get("token")
    email = email_from_token(token)
    (donor, first_receipt) = get_donor_from_receipt_id(int(receipt_id))
    if donor["email"] == email:
        return produce_dons_pdf(first_receipt)
    return abort(404)


@bp.route("/grouped_receipts/<year>", methods=["GET"])
def grouped_receipts(year):
    """The PDF receipts for a year"""
    token = request.args.get("token")
    email = email_from_token(token)
    return produce_grouped_receipts(email, year)


@bp.route("/report", methods=["GET", "POST"])
def report_issue():
    """The report issue form"""
    token = request.args.get("token")
    if request.method == "POST":
        email_donateur = request.form["email"].strip()
        subject = request.form["subject"]
        if subject == "wrong_personal_informations":
            subject = "Erreur dans mes informations personnelles"
        elif subject == "missing_receipt":
            subject = "Reçus manquants ou incorrects"
        else:
            subject = request.form["subject_extra"]
        message = request.form["message"].strip()
        send_mail("rt+soutenir@framasoft.org", subject, message, email_donateur)
        flash(
            "Votre signalement a bien été envoyé. "
            f"Nous vous recontacterons à l'adresse : {email_donateur}"
        )
        return redirect(url_for("user.receipts", token=token))
    subject = request.args.get("subject")
    email = email_from_token(token)
    return render_template("report.html", subject=subject, email=email, token=token)
