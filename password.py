"""Generates a password hash from a password"""

import sys
from werkzeug.security import generate_password_hash

args = sys.argv
if len(args) > 1:
    print(generate_password_hash(args[1]))
else:
    print("Needs a password as argument")
