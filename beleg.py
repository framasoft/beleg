"""
Main module for Beleg, declaration of config, filters
"""

import logging
from os import getenv
from datetime import datetime, date
from flask import Flask, render_template, g
from babel.dates import format_date
from lxml import html
from num2words import num2words
from dotenv import load_dotenv
from werkzeug.middleware.proxy_fix import ProxyFix
from db import init_db_command
import admin
import user
from filters import format_currency

load_dotenv()

app = Flask(__name__)
app.secret_key = getenv("SECRET_KEY")

app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1)

app.config["PREFERRED_URL_SCHEME"] = getenv("PREFERRED_URL_SCHEME", "http")
app.config["SERVER_NAME"] = getenv("SERVER_NAME", "recu-dons.framasoft.org")

logging.basicConfig(
    filename="beleg.log", encoding="utf-8", level=int(getenv("LOGLEVEL", str(logging.INFO)))
)

app.register_blueprint(admin.bp)
app.register_blueprint(user.bp)


@app.teardown_appcontext
def close_connection(exception=None):  # pylint: disable=unused-argument
    """Close the database connction"""
    db = getattr(g, "_database", None)
    if db is not None:
        db.close()


app.teardown_appcontext(close_connection)
app.cli.add_command(init_db_command)


@app.context_processor
def inject_today_date():
    """Get today's date"""
    return {"today_date": format_date(date.today(), locale="fr")}


@app.template_filter("format_date_filter")
def format_date_filter(value):
    """Format a date"""
    return format_date(datetime.strptime(value, "%Y-%m-%d"), locale="fr")


@app.template_filter("format_date_2_filter")
def format_date_2_filter(value):
    """Format a short date"""
    return format_date(datetime.strptime(value, "%Y-%m-%d"), locale="fr", format="short")


@app.template_filter("format_currency")
def format_currency_filter(value):
    """Format a currency"""
    return format_currency(value)


@app.template_filter("nl2br")
def nl2br(value):
    """Replace new lines with <br>"""
    return value.replace("\n", "<br>")


@app.template_filter("html_escape")
def html_escape(value):
    """Escape HTML strings"""
    if value:
        tree = html.fromstring(value)
        return tree.text_content().strip()
    return ""


@app.template_filter("number_to_words")
def number_to_words(value):
    """Conver a number to words in french"""
    return num2words(value, lang="fr")


@app.errorhandler(404)
def not_found(error):  # pylint: disable=unused-argument
    """Handle 404"""
    return render_template("errors/404.html"), 404


@app.errorhandler(500)
def internal_error(error):  # pylint: disable=unused-argument
    """Handle 500"""
    return render_template("errors/500.html"), 500
