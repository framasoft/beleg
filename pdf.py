"""PDF export module"""

import logging
from flask import render_template
from weasyprint import HTML, CSS
from weasyprint.text.fonts import FontConfiguration


def return_pdf_receipt(template, **kwargs):
    """Render a PDF receipt"""
    logging.debug("Rendering donation PDF")

    font_config = FontConfiguration()
    html = generate_pdf_receipt(template, **kwargs)

    return html.render(stylesheets=[__generate_css(font_config)], font_config=font_config)


def generate_pdf_receipt(template, **kwargs):
    """Return a PDF receipt HTTP response"""
    logging.debug("Rendering PDF page")

    rendered_html = render_template(template, **kwargs)
    return HTML(string=rendered_html)


def __generate_css(font_config):
    return CSS(
        string="""
    @font-face {
        font-family: Roboto;
        src: url("https://framasoft.org/nav/fonts/Roboto-Regular.woff2") format('woff2');
        font-style: normal;
        font-weight: 400;
    }
    @font-face {
        font-family: Roboto;
        src: url("https://framasoft.org/nav/fonts/Roboto-LightItalic.woff2") format("woff2");
        font-style: italic;
        font-weight: 300;
    }
    @font-face {
        font-family: Roboto;
        src: url("https://framasoft.org/nav/fonts/Roboto-Medium.woff2") format("woff2");
        font-style: normal;
        font-weight: 500;
    }
    @font-face {
        font-family: Roboto;
        src: url("https://framasoft.org/nav/fonts/Roboto-Bold.woff2") format("woff2");
        font-style: normal;
        font-weight: 700;
    }
    @font-face {
        font-family: Roboto;
        src: url("https://framasoft.org/nav/fonts/Roboto-BoldItalic.woff2") format("woff2");
        font-style: italic;
        font-weight: 700;
    }
    @font-face {
        font-family: Roboto;
        src: url("https://framasoft.org/nav/fonts/Roboto-Black.woff2") format("woff2");
        font-style: normal;
        font-weight: 900;
    }
    body {
        font-family: Roboto !important;
    }
    @page {
        margin: 1cm;
    }
    """,
        font_config=font_config,
    )


class PDFException(Exception):
    "Raised when something goes wrong with generating PDFs"
