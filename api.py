"""XML-RPC API"""

import xmlrpc.client
from os import getenv
import logging


def get_partners(email):
    """List partners for a given email"""
    return __execute_kw(
        "res.partner",
        "search_read",
        [[["email", "=", email]]],
        {
            "fields": [
                "donation_count",
                "donation_ids",
                "contact_address",
                "lastname",
                "firstname",
                "display_name",
                "email",
                "street",
                "street2",
                "zip",
                "city",
                "country_id",
                "is_company",
                "siret",
                "statut_legal",
            ],
            "limit": 5,
        },
    )


def get_partner(partner_id):
    """Get a partner by it's ID"""
    return __execute_kw(
        "res.partner",
        "read",
        [[partner_id]],
        {
            "fields": [
                "donation_count",
                "donation_ids",
                "contact_address",
                "street",
                "street2",
                "zip",
                "city",
                "country_id",
                "lastname",
                "firstname",
                "display_name",
                "email",
                "is_company",
                "siret",
                "statut_legal",
            ]
        },
    )


def get_donator_partners_for_year(year, offset=0, limit=100000):
    """Get donator partners for a given year"""
    return __execute_kw(
        "res.partner",
        "search_read",
        [
            [
                ["donor_rank", ">", 0],
                ["donation_ids.donation_date", ">=", f"{year}-01-01"],
                ["donation_ids.donation_date", "<=", f"{year}-12-31"],
            ]
        ],
        {"fields": ["email", "display_name"], "limit": limit, "offset": offset},
    )


def count_donator_partners_for_year(year):
    """Count donator partners for a given year"""
    return __execute_kw(
        "res.partner",
        "search_count",
        [
            [
                ["donor_rank", ">", 0],
                ["donation_ids.donation_date", ">=", f"{year}-01-01"],
                ["donation_ids.donation_date", "<=", f"{year}-12-31"],
            ]
        ],
        {},
    )


def get_receipts(email):
    """Get all the receipts for a given email"""
    return __execute_kw(
        "donation.tax.receipt",
        "search_read",
        [[["partner_id.email_normalized", "ilike", email]]],
        {
            "fields": [
                "number",
                "partner_id",
                "date",
                "donation_date",
                "type",
                "amount",
                "currency_id",
                "donation_ids",
            ],
            "limit": 1000,
        },
    )


def get_receipts_by_email_and_year(email, year):
    """Get all receipts by email and by year"""
    return __execute_kw(
        "donation.tax.receipt",
        "search_read",
        [
            [
                "&",
                ["partner_id.email_normalized", "ilike", email],
                ["donation_date", ">=", f"{year}-01-01"],
                ["donation_date", "<=", f"{year}-12-31"],
            ]
        ],
        {
            "fields": [
                "number",
                "partner_id",
                "date",
                "donation_date",
                "type",
                "amount",
                "currency_id",
                "donation_ids",
            ],
            "limit": 1000,
        },
    )


def get_receipt(receipt_id):
    """Get a receipt by it's ID"""
    return __execute_kw(
        "donation.tax.receipt",
        "read",
        [[receipt_id]],
        {
            "fields": [
                "number",
                "partner_id",
                "date",
                "donation_date",
                "type",
                "amount",
                "currency_id",
                "donation_ids",
            ]
        },
    )


def get_donation(donation_id):
    """Get a donation by it's ID"""
    return __execute_kw(
        "donation.donation",
        "read",
        [[donation_id]],
        {
            "fields": [
                "amount_total",
                "number",
                "partner_id",
                "donation_date",
                "currency_id",
                "payment_mode_id",
                "payment_ref",
            ]
        },
    )


def __get_models():
    return xmlrpc.client.ServerProxy(f"{getenv('ODOO_ENDPOINT')}/xmlrpc/2/object", verbose=False)


def __execute_kw(model, action, filters, options):
    try:
        odoo_db = getenv("ODOO_DATABASE")
        user_id = int(getenv("ODOO_USER_ID"))
        password = getenv("ODOO_USER_PASSWORD")
        logging.debug(
            "Executing XML-RPC request with %s, %s, %s, %s", odoo_db, user_id, model, action
        )
        return __get_models().execute_kw(
            odoo_db, user_id, password, model, action, filters, options
        )
    except Exception as e:
        raise APIException("Odoo XML-RPC issue") from e


class APIException(Exception):
    "Raised when something goes wrong with interacting with Odoo APIs"
