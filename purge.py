"""Module to purge obsolete tokens"""

from flask import Flask
from db import init_db
from service import delete_obsolete_tokens

app = Flask(__name__)

with app.app_context():
    init_db()
    delete_obsolete_tokens()
