"""CLI to send receipts"""

import sys
import re
import click
from api import (
    APIException,
    get_donator_partners_for_year,
    count_donator_partners_for_year,
    get_partners,
    get_receipts_by_email_and_year,
)
from email_service import MailException, send_yearly_donation_email
from pdf import PDFException
from service import (
    generate_multiple_receipts_pdf,
    is_email_already_sent_for_year,
    remove_emails_sent_for_year,
    remove_emails_sent_for_year_and_email,
    save_year_sent_email,
    total_donations_for_receipts,
)
from beleg import app


class EmailParamType(click.ParamType):
    """A param matching the format of an email"""

    name = "email"

    def convert(self, value, param, ctx):  # pylint: disable=inconsistent-return-statements
        if is_email(value):
            return value
        self.fail(f"{value!r} is not a valid email", param, ctx)


EMAIL = EmailParamType()


@click.command()
@click.option("-c", "--count", is_flag=True, help="Return the number of emails to send")
@click.option(
    "-d",
    "--dry-run",
    is_flag=True,
    help="Only list the emails to be send, don't actually send them.",
)
@click.option("-v", "--verbose", is_flag=True, help="Give details on the donator being processes")
@click.option(
    "-f", "--force", is_flag=True, help="Force sending email even though we already sent them"
)
@click.option("--purge-year", is_flag=True, help="Remove all email sent status for given year")
@click.argument("year")
@click.argument("email", required=False, type=EMAIL)
def send_receipts(count, dry_run, verbose, force, purge_year, year, email):
    """Sending the receipts"""
    if re.match(r"[0-9]{4}", year) is None:
        click.secho("⚠️ Year must be a 4 digit value!", bg="red")
        sys.exit(1)

    with app.app_context():
        if purge_year:
            if email is not None:
                remove_emails_sent_for_year_and_email(year, email)
                click.echo(f"Removed email sent statuses for year {year} and address {email}")
            else:
                remove_emails_sent_for_year(year)
                click.echo(f"Removed all email sent statuses for year {year}")
            sys.exit(0)

        if email is not None:
            partners = get_partners(email)
            __process_donator(year, email, partners[0]["display_name"], dry_run, verbose, force)
            sys.exit(0)

        donators_count = count_donator_partners_for_year(year)
        if count:
            click.echo(donators_count)
            sys.exit(0)
        if donators_count == 0:
            click.echo("No donators found for this year, nothing sent")
            sys.exit(0)
        else:
            click.secho(f"Starting to send {donators_count} receipts for year {year}")
            donators = get_donator_partners_for_year(year)
            click.secho(f"Loaded list of all donators for year {year}")
            if verbose:
                __process_donators(year, donators, dry_run, verbose, force)
            else:
                with click.progressbar(donators) as progress_bar:
                    __process_donators(year, progress_bar, dry_run, verbose, force)


def __process_donators(year, donators, dry_run, verbose, force):
    for donator in donators:
        email = donator["email"]
        if not is_email(email):
            click.echo(f'Email address "{email}" is invalid, discarding it')
            continue
        try:
            __process_donator(year, email, donator["display_name"], dry_run, verbose, force)
        except PDFException:
            click.secho(
                f"An error occured while generating PDF document for {email}", err=True, bg="red"
            )
        except APIException:
            click.secho(
                f"An error occured while interacting with API for {email}", err=True, bg="red"
            )
        except MailException:
            click.secho(f"An error occured while sending mail for {email}", err=True, bg="red")


def __process_donator(year, email, display_name, dry_run, verbose, force):
    if is_email_already_sent_for_year(year, email) or force:
        click.echo(f"Donator with email {email} has already being sent the receipt for year {year}")
        return
    (total_amount, nb_valid_donations, pdf) = __produce_pdf_receipt(year, email)
    if pdf is None or nb_valid_donations == 0:
        click.echo(f"Donator with email {email} has no receipts for year {year}")
        return
    if dry_run and verbose:
        click.echo(f"Dry run: simulate sending {year} receipts to {email}")
        return

    send_yearly_donation_email(year, email, display_name, total_amount, nb_valid_donations, pdf)
    save_year_sent_email(year, email)
    if verbose:
        click.echo(f"Sent receipt to {email}")
    return


def __produce_pdf_receipt(year, email):
    receipts = get_receipts_by_email_and_year(email, year)
    if len(receipts) < 1:
        return (None, None, None)
    (total_amount, nb_valid_donations) = total_donations_for_receipts(receipts)
    return (total_amount, nb_valid_donations, generate_multiple_receipts_pdf(receipts, year))


def is_email(email) -> bool:
    """Whether the string is an email"""
    return isinstance(email, str) and email != "NC" and re.match(r"[^@]+@[^@]+\.[^@]+", email)


if __name__ == "__main__":
    send_receipts()  # pylint: disable=no-value-for-parameter
