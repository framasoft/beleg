"""Filters module"""


def format_currency(value):
    """Format a currency value"""
    return "{:,.2f}€".format(value)  # pylint: disable=consider-using-f-string
