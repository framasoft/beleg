"""DB stuff"""

import sqlite3
import click
from flask import g, current_app

DATABASE = "beleg.db"


def get_db():
    """Get the database connection"""
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row
    return db


@click.command("init-db")
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo("Initialized the database.")


def init_db():
    """Initialize the database connection."""
    with current_app.app_context():
        db = get_db()
        with current_app.open_resource("schema.sql", mode="r") as f:
            db.cursor().executescript(f.read())
        db.commit()
